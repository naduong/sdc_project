import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt

from importlib import reload
import utils; reload(utils)
from utils import *

from skimage.feature import hog
from sklearn.preprocessing import StandardScaler


def resize_image(img, ratio=0.5, single_vector=True):
    """resize the input ratio and return either single vector or
    multi-dimensional images
    """
    if img is None:
        print ('[IN resize_image]: an input img is None.')
        return None

    height, width = img.shape[0 : 2]
    features = cv2.resize(img, (int(height * ratio), int(width * ratio)))
    return features.ravel() if single_vector else features


def get_color_histogram(img, nbins=32, bins_range=(0, 256)):
    """
    Return the histograms of the color image across all channels, as a concatenanted feature vector
    """
    # Compute the histogram of the color channels separately
    channel1_hist = np.histogram(img[:,:,0], bins=nbins, range=bins_range)
    channel2_hist = np.histogram(img[:,:,1], bins=nbins, range=bins_range)
    channel3_hist = np.histogram(img[:,:,2], bins=nbins, range=bins_range)
    # Concatenate the histograms into a single feature vector
    hist_features = np.concatenate((channel1_hist[0], channel2_hist[0], channel3_hist[0]))
    # Return the individual histograms, bin_centers and feature vector
    return hist_features


def get_features(sub_sample_feature, color_hist_feature, feat_hog_feature,
                  use_sub_sample=False, use_color_hist=True, use_hog=True):
    """
    Returns a feature vector depending on feature flags ()
    """
    feature_vec = []
    if use_sub_sample:
        feature_vec = sub_sample_feature
    if use_color_hist:
        feature_vec = color_hist_feature if len(feature_vec) == 0 else np.hstack((feature_vec, color_hist_feature))
    if use_hog:
        feature_vec = feat_hog_feature if len(feature_vec) == 0 else np.hstack((feature_vec, feat_hog_feature))

    return feature_vec


def get_hog_features(img, orient, pix_per_cell, cell_per_block,
                        vis=False, feature_vec=True, b_norm="L2-Hys"):
    """
    returns hog feature of an image.
    Reference to hog feature: http://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.hog
    """
    if vis == True:
        features, hog_image = hog(img,
                                  orientations=orient,
                                  pixels_per_cell=(pix_per_cell, pix_per_cell),
                                  cells_per_block=(cell_per_block, cell_per_block),
                                  transform_sqrt=False,
                                  visualise=vis,
                                  feature_vector=feature_vec,
                                  block_norm=b_norm)
        return features, hog_image
    # Otherwise call with one output
    else:
        features = hog(img,
                       orientations=orient,
                       pixels_per_cell=(pix_per_cell, pix_per_cell),
                       cells_per_block=(cell_per_block, cell_per_block),
                       transform_sqrt=False,
                       visualise=vis,
                       feature_vector=feature_vec,
                       block_norm=b_norm)
        return features


def compute_features(img,
                     color_space="YCrCb",
                     subsample_size=(32, 32),
                     ratio=0.5,
                     hist_bins=32,
                     hist_range=(0, 256),
                     hog_orient=9,
                     hog_pix_per_cell=8,
                     hog_cells_per_block=2,
                     hog_block_norm="L2-Hys",
                     hog_color_channels=[0, 1, 2],
                     hog_grayscale=False):
    """

    """
    img_cspace = change_color_space(img, cspace=color_space)
    img_sub_sample = resize_image(img_cspace, ratio = ratio)
    img_color_hist = get_color_histogram(img_cspace, nbins=hist_bins, bins_range=hist_range)

    hog_features = []
    if hog_color_channels is None:
        hog_features, img_hog = hog(to_grayscale(img),
                                    orientations=hog_orient,
                                    pixels_per_cell=(hog_pix_per_cell, hog_pix_per_cell),
                                    cells_per_block=(hog_cells_per_block, hog_cells_per_block),
                                    block_norm=hog_block_norm,
                                    visualise=True,
                                    feature_vector=True)
    else:
        for ch in hog_color_channels:
            hf, img_hog = hog(img_cspace[:, :, ch],
                              orientations=hog_orient,
                              pixels_per_cell=(hog_pix_per_cell, hog_pix_per_cell),
                              cells_per_block=(hog_cells_per_block, hog_cells_per_block),
                              block_norm=hog_block_norm,
                              visualise=True,
                              feature_vector=True)
            hog_features = hf if len(hog_features) == 0 else np.concatenate((hog_features, hf))

    return get_features(img_sub_sample, img_color_hist, hog_features)


def normalise_features(features, normaliser=None):
    """
    Returns the tuple (normalised_features, normaliser),
    where normaliser can normalise feature data by applying the function normaliser.transform(data).
    The function can accept a default normaliser. If None is provided, one will be created and returned
    """
    stacked_features = np.vstack(features).astype(np.float64)
    if normaliser == None:
        normaliser = StandardScaler().fit(stacked_features)
    normed_features = normaliser.transform(stacked_features)

    return normed_features, normaliser


def get_features_from_config(image, configuration):
    """
    Returns the tuple of the image in the configured color space, and a list of hog visualisations for each color channel
    """
    hog_visualisations = []
    cspace_img = change_color_space(image, configuration.color_space)

    for ch in [0, 1, 2]:
        _, hog_vis = get_hog_features(cspace_img[:, :, ch],
                                      configuration.hog_orientations,
                                      configuration.hog_pixels_per_cell,
                                      configuration.hog_cells_per_block,
                                      b_norm=configuration.hog_block_norm,
                                      vis=True)

        hog_visualisations.append(hog_vis)

    return cspace_img, hog_visualisations


def get_hog_features_from_multiple_configs(img, configs):
    """
    Returns the tuple (features, labels) resulting from the extraction of hog features for the passed configs
    """
    features = []
    labels = []
    for config in configs:
        cspace_img, hog_imgs = get_features_from_config(img, config)
        features.append([cspace_img, hog_imgs[0], hog_imgs[1], hog_imgs[2]])
        hog_config_str = "o={0}, px/c={1}, c/bk={2}".format(config.hog_orientations,
                                                            config.hog_pixels_per_cell,
                                                            config.hog_cells_per_block)
        labels.append([config.color_space,
                       "[1]: {0}".format(hog_config_str),
                       "[2]: {0}".format(hog_config_str),
                       "[3]: {0}".format(hog_config_str)])

    return features, labels



