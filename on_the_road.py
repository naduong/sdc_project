import math
from utils import *
from feature_extraction import *
from scipy.ndimage.measurements import label


def multi_scale_pipeline(img, x_start, x_stop, y_start, y_stop, scale, cfier, normaliser,
                         orient, pix_per_cell, cell_per_block, spatial_size, hist_bins,
                         window=64, color_space="YCrCb", correct_label="vehicle", overlap_cells=4):

    draw_img = np.copy(img)
    # Normalise our input image
    img = change_color_space(img, cspace=color_space).astype(np.float32)

    # Define the image portion we are interested in
    img_tosearch = img[y_start:y_stop, x_start:x_stop, :]
    ctrans_tosearch = img_tosearch

    # Subsample this search section if needed
    if scale != 1:
        imshape = ctrans_tosearch.shape
        ctrans_tosearch = cv2.resize(ctrans_tosearch, (np.int(imshape[1] / scale), np.int(imshape[0] / scale)))

    ch1 = ctrans_tosearch[:, :, 0]
    ch2 = ctrans_tosearch[:, :, 1]
    ch3 = ctrans_tosearch[:, :, 2]

    # Define the total number of cells in a given direction (x, y)
    # This is equivalent to the number of convolutions per axis in deep learning (c.f. cs231n)
    # nxblock = [(axis_size - block_size) / stride] + 1
    #  -> [(axis_size - pix_per_cell * cells_per_block) / pix_per_cell] + 1
    #  -> [(axis_size / pix_per_cell) - cells_per_block] + 1
    nxblocks = (ch1.shape[1] // pix_per_cell) - cell_per_block + 1
    nyblocks = (ch1.shape[0] // pix_per_cell) - cell_per_block + 1
    # Each cell within a block is normalised and produces a histogram of oriented features
    nfeat_per_block = orient * cell_per_block

    # 64x64 was the orginal sampling rate
    # Same here - we use the convolution formula to figure out the number of blocks in the window
    nblocks_per_window = (window // pix_per_cell) - cell_per_block + 1

    # Define how many steos we overlap
    cells_per_step = overlap_cells

    nxsteps = (nxblocks - nblocks_per_window) // cells_per_step + 1
    nysteps = (nyblocks - nblocks_per_window) // cells_per_step + 1

    # Compute individual channel HOG features for the entire image
    hog1 = get_hog_features(ch1, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog2 = get_hog_features(ch2, orient, pix_per_cell, cell_per_block, feature_vec=False)
    hog3 = get_hog_features(ch3, orient, pix_per_cell, cell_per_block, feature_vec=False)

    bounding_boxes = []
    all_windows = []
    for xb in range(nxsteps):
        xpos = xb * cells_per_step
        for yb in range(nysteps):
            ypos = yb * cells_per_step
            # Extract HOG for this patch
            hog_feat1 = hog1[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
            hog_feat2 = hog2[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
            hog_feat3 = hog3[ypos:ypos + nblocks_per_window, xpos:xpos + nblocks_per_window].ravel()
            hog_features = np.hstack((hog_feat1, hog_feat2, hog_feat3))

            xleft = xpos * pix_per_cell
            ytop = ypos * pix_per_cell

            # Extract the image patch
            subimg = cv2.resize(ctrans_tosearch[ytop:ytop + window, xleft:xleft + window], (window, window))

            # Get color features
            spatial_features = resize_image(subimg)
            hist_features = get_color_histogram(subimg, nbins=hist_bins)

            # Scale features and make a prediction
            picked_features = get_features(spatial_features, hist_features, hog_features)

            if len(picked_features.shape) == 1:
                picked_features = picked_features.reshape(1, -1)

            test_features = normaliser.transform(picked_features)
            test_prediction = cfier.predict(test_features)

            # Compute our window's actual size
            xbox_left = np.int(xleft * scale)
            ytop_draw = np.int(ytop * scale)
            win_draw = np.int(window * scale)

            top_left = (xbox_left + x_start, ytop_draw + y_start)
            bottom_right = (xbox_left + win_draw + x_start, ytop_draw + win_draw + y_start)

            all_windows.append((top_left, bottom_right))
            if test_prediction == correct_label:
                bounding_boxes.append((top_left, bottom_right))

    return bounding_boxes, all_windows


def create_heatmap(img, bbox_list):
    """
    Returns a new heatmap where the regions of the image captured by the bounding boxes are "heated-up"
    """
    heatmap = np.zeros_like(img[:, :, 0]).astype(np.float)
    for box in bbox_list:
        heatmap[box[0][1]:box[1][1], box[0][0]:box[1][0]] += 1

    heatmap = np.clip(heatmap, 0, 255)

    return heatmap


def threshold_heatmap(heatmap, threshold=2):
    """
    Updates the heatmap by only retaining hot sections whose values are strictly above the threshold.
    All other values are "cooled" down
    """
    heatmap_copy = np.copy(heatmap)
    # Zero out pixels below the threshold
    heatmap_copy[heatmap_copy < threshold] = 0
    # Return thresholded map
    return heatmap_copy


def label_heatmap(heatmap):
    """
    Returns a slice with labels over the original heatmap, as well as the total count of labels found
    """
    return label(heatmap)


def find_smoothed_boxes(img, labels, duplicate=True, min_area_filter=32 * 32):
    """
    Returns a set of bounding boxes where each bounding box is the aggregations of neibhouring boxes for the same label
    """
    aggregate_bboxes = []
    # Iterate through all detected cars
    for car_number in range(1, labels[1] + 1):
        # Find pixels with each car_number label value
        nonzero = (labels[0] == car_number).nonzero()
        # Identify x and y values of those pixels
        nonzeroy = np.array(nonzero[0])
        nonzerox = np.array(nonzero[1])

        top_left = (np.min(nonzerox), np.min(nonzeroy))
        bottom_right = (np.max(nonzerox), np.max(nonzeroy))

        if (bottom_right[0] - top_left[0]) * (bottom_right[1] - top_left[1]) < min_area_filter:
            continue

        # Define a bounding box based on min/max x and y
        aggregate_bboxes.append(((np.min(nonzerox), np.min(nonzeroy)), (np.max(nonzerox), np.max(nonzeroy))))
    # Return the image

    return aggregate_bboxes


def smoothen_bounding_boxes(img, bboxes, thres=2):
    heat = create_heatmap(img, bboxes)
    heat_thresholded = threshold_heatmap(heat, threshold=thres)
    heat_labels = label_heatmap(heat_thresholded)
    smoothed_bboxes = find_smoothed_boxes(img, heat_labels)

    return heat, heat_thresholded, heat_labels, smoothed_bboxes


def create_heatmap_rgb_img(hmap):
    """
    Create a heatmap from a binary image
    """
    scale = np.max(hmap) / 255
    if scale == 0:
        # Handle the zero case. We could also add an epsilon
        scale = 1
    z = np.zeros_like(hmap).astype(np.uint8)
    hm_scaled = (hmap / scale).astype(np.uint8)
    return np.dstack((hm_scaled, z, z))


classifier = load_as_pickle("classifier_Grid_Search_SVC_Hist_ch0_HOG-11-14-2_YCrCb.p")
normaliser = load_as_pickle("normaliser_Hist_ALL_HOG-11-14-2_YCrCb.p")


# prepare test image here
test_imgs_dir = 'test'
test_imgs_paths = glob.glob(test_imgs_dir + "/*.jpg", recursive=True)
test_imgs = np.asarray(list(map(lambda img_path: load_image(img_path), test_imgs_paths)))

print (test_imgs_paths)

mc_bboxes, wins = multi_scale_pipeline(test_imgs[5], 64, 1280, 400, 656, 4, classifier, normaliser, 11, 14, 2, [32, 32], 32, color_space="YCrCb", window=64, overlap_cells=4)
new_img_boxes = draw_boxes(test_imgs[5], mc_bboxes)
new_img_all_windows = draw_boxes(test_imgs[5], wins, alternating_bbox_color=(255, 153, 0))
plt.figure(figsize=(12,8))
plt.imshow(new_img_all_windows)

heatmap, heatmap_thres, heatmap_lbs, smoothed_bboxes = smoothen_bounding_boxes(test_imgs[5], mc_bboxes, thres=1)
plt.imshow(draw_boxes(test_imgs[5], smoothed_bboxes))