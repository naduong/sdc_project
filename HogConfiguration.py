class HogConfiguration:
    """
    Stores feature configuration
    """
    def __init__(self,
                 color_space,
                 hog_orientations,
                 hog_pixels_per_cell,
                 hog_cells_per_block,
                 hog_block_norm,
                 hog_color_channels=[0,1,2]):

        self.color_space = color_space
        self.hog_orientations = hog_orientations
        self.hog_pixels_per_cell = hog_pixels_per_cell
        self.hog_cells_per_block = hog_cells_per_block
        self.hog_block_norm = hog_block_norm
        self.hog_color_channels = hog_color_channels