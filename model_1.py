from feature_extraction import *
import HogConfiguration
from importlib import reload
import utils; reload(utils)
from utils import *

from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import GridSearchCV


def extract_dataset_features(classes_imgs, labels, config):
    """
    """
    # container for features
    all_features = []

    # container for labels
    all_labels = []

    for i, class_imgs in enumerate(classes_imgs):
        class_features = []
        class_labels = np.repeat(labels[i], len(class_imgs))
        all_labels = class_labels if len(all_labels) == 0 else np.concatenate((all_labels, class_labels))
        for class_img_path in class_imgs:
            img = load_image(class_img_path)
            img_features = compute_features(img,
                                            color_space=config.color_space,
                                            hog_orient=config.hog_orientations,
                                            hog_pix_per_cell=config.hog_pixels_per_cell,
                                            hog_cells_per_block=config.hog_cells_per_block,
                                            hog_color_channels=config.hog_color_channels)
            class_features.append(img_features)

        all_features.append(class_features)

    normed_features, normaliser = normalise_features(all_features)
    return normed_features, all_labels, normaliser


def split_dataset(data, labels, test_pct=0.2):
    """
    """
    X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=test_pct)
    return (X_train, y_train), (X_test, y_test)


def train_classifier(data, labels, method="LinearSVC"):
    """
    """
    cfier = None
    if method == "LinearSVC":
        cfier = LinearSVC(C=100)
    elif method == "RBFSVC":
        cfier = SVC(C=100)
    elif method == "DecisionTree":
        cfier = DecisionTreeClassifier()

    cfier.fit(data, labels)

    return cfier


def train_classifier_grid_search(data, labels, method="SVM"):
    parameters = {}
    cfier = None
    if method == "SVM":
        parameters = {'kernel': ('linear', 'rbf'), 'C': [1, 100, 1000, 10000], "gamma": ["auto", 0.01, 0.1, 1]}
        cfier = SVC()

    cfier_gs = GridSearchCV(cfier, parameters, n_jobs=2, verbose=5)
    cfier_gs.fit(data, labels)

    return cfier_gs

dataset_dir = "data/OwnCollection"
dataset_vehicles_dir = "data/OwnCollection/vehicles"
dataset_non_vehicles_dir = "data/OwnCollection/non-vehicles"

# Let's fetch the paths of training images
vehicles_path_list = get_image_path_list(dataset_vehicles_dir)
non_vehicles_path_list = get_image_path_list(dataset_non_vehicles_dir)

example_vehicle_imgs = get_sample_images(vehicles_path_list, sample_count=1)
example_non_vehicle_imgs = get_sample_images(non_vehicles_path_list, sample_count=1)

hog_config = HogConfiguration.HogConfiguration("YCrCb", 11, 14, 2, "L2-Hys", hog_color_channels=[0,1,2])

data_features, data_labels, normaliser = extract_dataset_features([vehicles_path_list, non_vehicles_path_list],
                                                                  ["vehicle", "non_vehicle"],
                                                                  hog_config)

save_as_pickle(data_features, "features_Hist_ALL_HOG-11-14-2_YCrCb.p")

save_as_pickle(data_labels, "data_labels_Hist_ALL_HOG-11-14-2_YCrCb.p")
save_as_pickle(normaliser, "normaliser_Hist_ALL_HOG-11-14-2_YCrCb.p")

normaliser = load_as_pickle("normaliser_Hist_ALL_HOG-11-14-2_YCrCb.p")
data_features = load_as_pickle("features_Hist_ALL_HOG-11-14-2_YCrCb.p")
data_labels = load_as_pickle("data_labels_Hist_ALL_HOG-11-14-2_YCrCb.p")

print (len(data_labels))

training_dataset, test_dataset = split_dataset(data_features, data_labels)
print(training_dataset[0].shape, training_dataset[1].shape)

print(test_dataset[0].shape, test_dataset[1].shape)
print ('Training classifier_grid_search...')
classifier_grid_search = train_classifier_grid_search(training_dataset[0], training_dataset[1])
save_as_pickle(classifier_grid_search, "classifier_Grid_Search_SVC_Hist_ch0_HOG-11-14-2_YCrCb.p")

print ('Training LinearSVC classifier...')
simple_classifier = train_classifier(training_dataset[0], training_dataset[1], method="LinearSVC")
save_as_pickle(simple_classifier, "linearSVC_classifier_SVC_Hist_ch0_HOG-11-14-2_YCrCb.p")

print ('Training LinearSVC classifier...')
better_classifier = train_classifier(training_dataset[0], training_dataset[1], method="RBFSVC")
save_as_pickle(better_classifier, "RBFSVC_classifier_Grid_Search_SVC_Hist_ch0_HOG-11-14-2_YCrCb.p")

print (classifier_grid_search.best_params_ss)

classifier = classifier_grid_search




