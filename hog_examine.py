from utils import *
from feature_extraction import *
import HogConfiguration
import os

color_channels = ['RGB', 'HSV', 'LUV', 'HLS', 'YUV', 'YCrCb', 'LAB']


def create_config_set(color_channel):
    return [
        HogConfiguration.HogConfiguration(color_channel, 9, 8, 2, "L2-Hys"),
        HogConfiguration.HogConfiguration(color_channel, 9, 10, 2, "L2-Hys"),
        HogConfiguration.HogConfiguration(color_channel, 9, 12, 2, "L2-Hys"),
        HogConfiguration.HogConfiguration(color_channel, 9, 14, 2, "L2-Hys"),
        HogConfiguration.HogConfiguration(color_channel, 9, 16, 2, "L2-Hys"),
    ]


dataset_dir = "data/OwnCollection"
dataset_vehicles_dir = "data/OwnCollection/vehicles"
dataset_non_vehicles_dir = "data/OwnCollection/non-vehicles"

# Let's fetch the paths of training images
vehicles_path_list = get_image_path_list(dataset_vehicles_dir)
non_vehicles_path_list = get_image_path_list(dataset_non_vehicles_dir)

example_vehicle_imgs = get_sample_images(vehicles_path_list, sample_count=100)
example_non_vehicle_imgs = get_sample_images(non_vehicles_path_list, sample_count=100)

save_non_vehicle_hog_path = 'non_vehicle_hog'
if not os.path.exists(save_non_vehicle_hog_path):
    os.makedirs(save_non_vehicle_hog_path)

for index in range(len(example_non_vehicle_imgs)):
    image = example_non_vehicle_imgs[index]

    for color_channel in color_channels:
        save_path = save_non_vehicle_hog_path + '/' + str(index) + '_' + color_channel + '.png'
        configs_1 = create_config_set(color_channel)

        non_vehicle_imgs_hog, non_vehicle_imgs_hog_labels = get_hog_features_from_multiple_configs(
            image, configs_1)

        show_image_list(non_vehicle_imgs_hog, non_vehicle_imgs_hog_labels,
                        color_channel + 'Figure',
                        cols=4, fig_size=(24, 24), show_ticks=False, save_name=save_path)
