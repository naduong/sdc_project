import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt


def get_image_path_list(dataset_vehicles_dir):
    if dataset_vehicles_dir is None:
        return None

    return glob.glob(dataset_vehicles_dir + "/**/*.png", recursive=True)


def get_sample_images(path_list, sample_count=5):
    if path_list is None:
        return None

    random_map = map(lambda path: load_image(path),
                     np.asarray(path_list)[np.random.randint(
                         0, high=len(path_list), size=sample_count)
                     ])

    return np.asarray(list(random_map))


##############################################################################
#################    IMAGE LOADING AND RENDERING FUNCTIONS   #################
##############################################################################


def load_image(image_path, to_rgb=True):
    """
    Load image from image_path in rgb chanel
    """
    img = cv2.imread(image_path)
    return img if not to_rgb else cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


def show_image_list(img_list, img_labels, title, cols=2, fig_size=(15, 15), show_ticks=True, save_name='None'):
    """
    Utility function to to show a list of images
    """
    rows = len(img_list)
    cmap = None

    fig, axes = plt.subplots(rows, cols, figsize=fig_size)

    for i in range(0, rows):
        for j in range(0, cols):
            ax = axes[i, j] if rows > 1 else axes[j]
            # k = i * cols + j
            img_name = img_labels[i][j]
            img = img_list[i][j]
            if len(img.shape) < 3 or img.shape[-1] < 3:
                cmap = "gray"
                img = np.reshape(img, (img.shape[0], img.shape[1]))

            if not show_ticks:
                ax.axis("off")

            ax.imshow(img, cmap=cmap)
            ax.set_title(img_name)

    fig.suptitle(title, fontsize=12, fontweight='bold', y=1)
    fig.tight_layout()

    if save_name is not None:
        fig.savefig(save_name)
    else:
        plt.show()

    plt.close(fig)

    return


##############################################################################
###################      IMAGE MODIFICATION FUNCTIONS      ###################
##############################################################################

def to_grayscale(img):
    """
    Returns the grayscaled version of the supplied image (in RGB format)
    """
    return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)


def to_hsv(img):
    """
    Returns the same image in HSV format
    The input image must be in RGB format
    """
    return cv2.cvtColor(img, cv2.COLOR_RGB2HSV)


def to_hls(img):
    """
    Returns the same image in HLS format
    The input image must be in RGB format
    """
    return cv2.cvtColor(img, cv2.COLOR_RGB2HLS)


def to_lab(img):
    """
    Returns the same image in LAB format
    Th input image must be in RGB format
    """
    return cv2.cvtColor(img, cv2.COLOR_RGB2LAB)


def to_ycrcb(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)


def to_yuv(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2YUV)


def to_luv(img):
    return cv2.cvtColor(img, cv2.COLOR_RGB2LUV)


def change_color_space(img, cspace="YCrCb"):
    """
    Takes in a RGB image and returns a new image in the target color space.
    If the current image and target image are both rgb, then a copy of the image is returned
    """
    feature_image = None
    if cspace != 'RGB':
        if cspace == 'HSV':
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
        elif cspace == 'LUV':
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2LUV)
        elif cspace == 'HLS':
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2HLS)
        elif cspace == 'YUV':
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)
        elif cspace == "YCrCb":
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2YCrCb)
        elif cspace == "LAB":
            feature_image = cv2.cvtColor(img, cv2.COLOR_RGB2LAB)
    else:
        feature_image = np.copy(img)

    return feature_image


def draw_boxes(img, bboxes, color=(0, 0, 255), thick=6, alternating_bbox_color=(0, 255, 0), copy=True):
    """
    Returns a new image the bounding boxes have been overlaid in the chosen color
    """
    imcopy = np.copy(img) if copy else img
    for i, bbox in enumerate(bboxes):
        c = color if i % 2 == 0 else alternating_bbox_color
        cv2.rectangle(imcopy, bbox[0], bbox[1], c, thick)

    return imcopy


##############################################################################
#################          MISCELLANEOUS FUNCTIONS           #################
##############################################################################

import pickle


def save_as_pickle(data, pickle_file_name):
    """
    Saves the data inside a pickle file
    """
    pickle.dump(data, open(pickle_file_name, "wb"))


def load_as_pickle(pickle_file_name):
    """
    Returns the loaded data inside the specified pickle file
    """
    return pickle.load(open(pickle_file_name, "rb"))
